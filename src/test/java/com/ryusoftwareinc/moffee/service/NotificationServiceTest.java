package com.ryusoftwareinc.moffee.service;

import java.io.File;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.ryusoftwareinc.moffee.notification.model.Email;
import com.ryusoftwareinc.moffee.notification.service.NotificationService;

@SpringBootTest
public class NotificationServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private NotificationService notifService;

	@Disabled
	@Test
	public void sendEmailSendEmail() {
		Email email = new Email();
		email.setSubject("test subject");
		email.setFrom("joseph.ryu1@gmail.com");
		email.setText("text body text");
		email.getTo().add("joseph.ryu1@gmail.com");
//		email.getCc().add("joseph.ryu1@gmail.com");
//		email.getBcc().add("joseph.ryu1@gmail.com");

		String path = "/Users/josephryu/Downloads/JosephRyu_Resume_2019.docx";
		File file = new File(path);
		email.getAttachments().add(file);

		notifService.sendEmail(email);
	}

}
