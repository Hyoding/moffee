package com.ryusoftwareinc.moffee.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@SpringBootTest
public class UserServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private UserService userService;

	private User testUser = null;

	@BeforeEach
	public void prepare() {
		testUser = new User();
		testUser.setFirstName("Joseph");
		testUser.setLastName("Ryu");
		testUser.setEmail("joseph.ryu1@gmail.com" + System.currentTimeMillis());
		testUser.setHashedPassword("pwd");
	}

	@Test
	public void checkEmailExistsShouldReturnTrueWhenEmailExists() {
		userService.save(testUser);
		boolean exists = userService.checkEmailExists(testUser.getEmail());
		assertTrue(exists);
	}

	@Test
	public void checkEmailExistsShouldReturnFalseWhenEmailDoesNotExists() {
		boolean exists = userService.checkEmailExists(testUser.getEmail());
		assertFalse(exists);
	}

}
