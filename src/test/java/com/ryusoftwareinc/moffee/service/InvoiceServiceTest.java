package com.ryusoftwareinc.moffee.service;

import static org.junit.jupiter.api.Assertions.*;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.ryusoftwareinc.moffee.EntityFactory;
import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.core.exception.ClientException;
import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.core.validator.ValidationException;
import com.ryusoftwareinc.moffee.invoice.Invoice;
import com.ryusoftwareinc.moffee.invoice.InvoiceService;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.service.SpotService;
import com.ryusoftwareinc.moffee.user.model.User;

@SpringBootTest
public class InvoiceServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private EntityFactory entityFactory;
	
	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private SpotService spotService;

	@Test
	public void invoiceShouldBeCreatedIfOccupiedForfirstTime() {
		User user = entityFactory.createUser();
		Spot spot = entityFactory.createSpot(user);

		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		Car car = parker.getCars().stream().findAny().get();
		long minToAdd = 45;

		Invoice invoice = invoiceService.findBySpotAndClosedDateNull(spot);
		assertNull(invoice);

		spotService.payMeter(spot, car, minToAdd);
		invoice = invoiceService.findBySpotAndClosedDateNull(spot);
		assertNotNull(invoice);
	}

	@Test
	public void onlyOneActiveInvoiceShouldExistForSpotAtGivenTime() {
		User user = entityFactory.createUser();
		Spot spot = entityFactory.createSpot(user);
		User parker = entityFactory.createParker();

		Invoice invoice = new Invoice();
		invoice.setSpot(spot);
		invoice.setParker(parker);
		invoiceService.save(invoice);

		Invoice invoice2 = new Invoice();
		invoice2.setSpot(spot);
		invoice2.setParker(parker);
		try {
			invoiceService.save(invoice2);
			fail("Should not reach here");
		} catch (ValidationException ex) {

		}

	}

	@Test
	@Disabled("Doesn't work in tests since Listener doesn't work in tests")
	public void invoiceShouldNotBeAllowedToHaveNegativeTotal() {
		User user = entityFactory.createUser();
		Spot spot = entityFactory.createSpot(user);

		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		Car car = parker.getCars().stream().findAny().get();
		long minToAdd = 45;

		spotService.payMeter(spot, car, minToAdd);
		Invoice invoice = invoiceService.findBySpotAndClosedDateNull(spot);
		invoice.setTotal(-100);
		try {
			invoiceService.save(invoice);
			fail("Should not be here");
		} catch (ValidationException ex) {

		}
	}

	@Test
	public void invoiceShouldBeCorrectlyUpdated() {
		User user = entityFactory.createUser();
		Spot spot = entityFactory.createSpot(user);
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		Car car = parker.getCars().stream().findAny().get();

		long minToAdd = 45;
		double initRate = spot.getRate();
		long minPaidTotal = 0;

		spotService.payMeter(spot, car, minToAdd);
		minPaidTotal += minToAdd;
		Invoice invoice = invoiceService.findBySpotAndClosedDateNull(spot);

		assertTrue(invoice.getAgreedRate() == initRate);
		assertTrue(invoice.getMinutesPaid() == minPaidTotal);

		spotService.payMeter(spot, car, minToAdd);
		minPaidTotal += minToAdd;

		assertTrue(invoice.getAgreedRate() == initRate);
		assertTrue(invoice.getMinutesPaid() == minPaidTotal);

		spot.setRate(initRate + 2);
		spotService.payMeter(spot, car, minToAdd);
		minPaidTotal += minToAdd;

		assertTrue(invoice.getAgreedRate() == initRate);
		assertTrue(invoice.getMinutesPaid() == minPaidTotal);
	}
	
	@Test
	public void closeInvoiceShouldPutinCloseDateAndCalculateBill() {
		User user = entityFactory.createUser();
		Spot spot = entityFactory.createSpot(user);
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		Car car = parker.getCars().stream().findAny().get();

		long minToAdd = 45;
		spotService.payMeter(spot, car, minToAdd);
		invoiceService.findBySpotAndClosedDateNull(spot);

		Invoice invoice = invoiceService.closeInvoice(spot);
		assertTrue(DateTimeUtil.almostEqual(invoice.getClosedDate(), DateTimeUtil.now()));
		assertTrue(invoice.getMinutesPaid() == minToAdd);
		assertTrue(invoice.getAgreedRate() == spot.getRate());
		assertTrue(invoice.getTotal() == minToAdd * spot.getRate());
		assertTrue(invoice.getTotal() >= 0);
		assertTrue(invoice.getProviderCutAmt() >= 0);
		assertTrue(invoice.getServiceCutAmt() >= 0);
	}
	
	@Test
	public void closeInvoiceShouldGiveMoneyToProvider() {
		User user = entityFactory.createUser();
		double initialAmtInBucket = user.getAmtInBucket();
		double initialAmtTotalEarned = user.getTotalEarned();
		
		Spot spot = entityFactory.createSpot(user);
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		Car car = parker.getCars().stream().findAny().get();

		long minToAdd = 45;
		spotService.payMeter(spot, car, minToAdd);
		invoiceService.findBySpotAndClosedDateNull(spot);
		Invoice invoice = invoiceService.closeInvoice(spot);
		
		double changedAmtInBucket = user.getAmtInBucket();
		double changedAmtTotalEarned = user.getTotalEarned();
		assertTrue(changedAmtInBucket == initialAmtInBucket + invoice.getProviderCutAmt());
		assertTrue(changedAmtTotalEarned ==  initialAmtTotalEarned + invoice.getProviderCutAmt());
	}
	
	@Test
	public void chargeTimeShouldTakeMoneyFromParkerCorrectly() {
		User user = entityFactory.createUser();		
		Spot spot = entityFactory.createSpot(user);
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		double initParkerBalance = parker.getBalance();
		Car car = parker.getCars().stream().findAny().get();
		
		long minutes = 15;
		
		// 1st time paying
		spotService.payMeter(spot, car, minutes);
		double balanceAfterPayMeter1 = parker.getBalance();		
		assertTrue(balanceAfterPayMeter1 == initParkerBalance - (spot.getRate() * (minutes / 60.0)));
		
		// 2nd time paying
		spotService.payMeter(spot, car, minutes);
		double balanceAfterPayMeter2 = parker.getBalance();		
		assertTrue(balanceAfterPayMeter2 == balanceAfterPayMeter1 - (spot.getRate() * (minutes / 60.0)));
	}
	
	@Test
	public void chareTimeShouldFailIfNotEnoughMoney() {
		User user = entityFactory.createUser();		
		Spot spot = entityFactory.createSpot(user);
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(spot, availUntil);

		User parker = entityFactory.createParker();
		parker.setBalance(0);
		Car car = parker.getCars().stream().findAny().get();
		
		long minutes = 15;
		
		// 1st time paying
		try {
			spotService.payMeter(spot, car, minutes);
			fail("Should not reach here");
		} catch (ClientException ex) {
			
		}
	}

}
