package com.ryusoftwareinc.moffee.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.car.service.CarService;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@SpringBootTest
public class CarServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private UserService userService;
	
	@Autowired
	private CarService carService;

	private User testUser = null;

	@BeforeEach
	public void prepare() {
		testUser = new User();
		testUser.setFirstName("Joseph");
		testUser.setLastName("Ryu");
		testUser.setEmail("joseph.ryu1@gmail.com" + System.currentTimeMillis());
		testUser.setHashedPassword("pwd");
	}

	@Test
	public void registerCarShouldAddCarToUser() {
		testUser = userService.save(testUser);
		Car car = new Car();
		car.setLicensePlate("testLicensePlate");
		car.setModel("testModel");
		car.setColour("testColour");
		carService.registerCar(car, testUser.getId());
		assertTrue(car.getId() != null);
	}

}
