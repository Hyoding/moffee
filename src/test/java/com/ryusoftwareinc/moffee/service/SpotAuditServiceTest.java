package com.ryusoftwareinc.moffee.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.Spot.SpotStatus;
import com.ryusoftwareinc.moffee.spot.model.SpotAudit;
import com.ryusoftwareinc.moffee.spot.service.SpotAuditService;
import com.ryusoftwareinc.moffee.spot.service.SpotService;

@SpringBootTest
public class SpotAuditServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private SpotAuditService spotAuditService;

	@Autowired
	private SpotService spotService;

	@Test
	public void auditShouldSaveRecord() {
		Spot spot = new Spot();
		spot = spotService.save(spot);

		SpotAudit audit1 = spotAuditService.audit(spot);
		spot.setStatus(SpotStatus.AVAILABLE);
		spot.setAvailableUntil(DateTimeUtil.now());
		spotAuditService.audit(spot);

		List<SpotAudit> spotAudits = (List<SpotAudit>) spotAuditService.findAllBySpot(spot);
		assertTrue(spotAudits.size() == 2);

		for (SpotAudit spotAudit : spotAudits) {
			assertTrue(spotAudit.getSpot().equals(spot));
			if (spotAudit.equals(audit1)) {
				assertTrue(spotAudit.getStatus() == SpotStatus.UNAVAILABLE);
				assertTrue(spotAudit.getAvailableUntil() == null);
			} else {
				assertTrue(spotAudit.getStatus() == SpotStatus.AVAILABLE);
				assertTrue(spotAudit.getAvailableUntil() != null);
			}
		}
	}

}
