package com.ryusoftwareinc.moffee.service;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.ZonedDateTime;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.web.multipart.MultipartFile;

import com.ryusoftwareinc.moffee.EntityFactory;
import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.core.model.AbstractFormEntity.FormStatus;
import com.ryusoftwareinc.moffee.core.model.Address;
import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.spot.exception.SpotNotOccupiedException;
import com.ryusoftwareinc.moffee.spot.exception.SpotOccupiedException;
import com.ryusoftwareinc.moffee.spot.exception.SpotUnavailableException;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.Spot.SpotStatus;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationForm;
import com.ryusoftwareinc.moffee.spot.service.SpotService;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@SpringBootTest
public class SpotServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	private static Logger logger = Logger.getLogger(SpotServiceTest.class);
	
	@Autowired
	private EntityFactory entityFactory;

	@Autowired
	private SpotService spotService;

	@Autowired
	private UserService userService;

	private Spot testSpot = null;
	private User testUser = null;
	
	private Car car = null;

	@BeforeEach
	public void prepare() {
		testUser = entityFactory.createUser();
		testSpot = entityFactory.createSpot(testUser);
		car = entityFactory.createCar();
	}

	/*
	 * 
	 * makeSpotAvailable fail: - SPOT with status OCCUPIED and availableUntil before
	 * occupiedUntil availableUntil
	 * 
	 * pass: - Spot with status UNAVAILABLE
	 * 
	 */

	@Test
	public void makeSpotAvailableShouldHaveCorrectStatusAvailableUntilTimeUnderNormalCondition() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(2);
		spotService.makeSpotAvailable(testSpot, availUntil);

		assertTrue(testSpot.getStatus() == Spot.SpotStatus.AVAILABLE);
		assertTrue(testSpot.getAvailableUntil().isEqual(availUntil));
		assertTrue(testSpot.getParker() == null);
		assertTrue(testSpot.getOccupyUntil() == null);
	}

	@Test
	public void makeSpotAvailableShouldThrowExceptionWhenStatusOccupiedAndNewAvailableUntilBeforeExistingAvailableUntile() {
		ZonedDateTime initialAvailUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, initialAvailUntil);

		int minToAdd = 30;
		spotService.payMeter(testSpot, car, minToAdd);

		ZonedDateTime newAvailUntil = testSpot.getOccupyUntil().minusHours(1);
		try {
			spotService.makeSpotAvailable(testSpot, newAvailUntil);
			fail("Should not reach here");
		} catch (SpotOccupiedException ex) {
		}
	}

	/*
	 * 
	 * makeSpotUnavailable fail: - Spot with status OCCUPIED pass: - Any other case
	 * 
	 */
	@Test
	public void makeSpotUnavailableShouldBeSuccessfulAndHaveCorrectStateUnderNormalCondition() {
		spotService.makeSpotUnavailable(testSpot);
		assertTrue(testSpot.getStatus() == Spot.SpotStatus.UNAVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == null);
		assertTrue(testSpot.getParker() == null);
		assertTrue(testSpot.getOccupyUntil() == null);

		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(2);
		spotService.makeSpotAvailable(testSpot, availUntil);

		spotService.makeSpotUnavailable(testSpot);
		assertTrue(testSpot.getStatus() == Spot.SpotStatus.UNAVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == null);
		assertTrue(testSpot.getParker() == null);
		assertTrue(testSpot.getOccupyUntil() == null);
	}

	@Test
	public void makeSpotUnavailableShouldNotBeSuccessfulIfStatusIsOccupied() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);
		spotService.payMeter(testSpot, car, 30);

		try {
			spotService.makeSpotUnavailable(testSpot);
			fail("Should not reach here");
		} catch (SpotOccupiedException ex) {
		}
		
	}

	/*
	 * 
	 * occupySpot fail: - Spot with status OCCUPIED - Spot with status AVAILABLE
	 * that has availableUntil before occupyUntil - Spot with status UNAVAILABLE
	 * pass: - Any other case
	 * 
	 */
	@Test
	public void occupySpotShouldFailIfSpotStatusIsUnavailable() {
		ZonedDateTime occupyUntil = DateTimeUtil.now().plusHours(5);
		int minToAdd = DateTimeUtil.differenceInMin(DateTimeUtil.now(), occupyUntil);
		try {
			spotService.payMeter(testSpot, car, minToAdd);
			fail("Should not reach here");
		} catch (SpotUnavailableException ex) {
		}
	}

	@Test
	public void occupySpotShouldAllowExtension() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);

		int minToAdd = 30;
		spotService.payMeter(testSpot, car, minToAdd);
		ZonedDateTime initOccUntil = testSpot.getOccupyUntil();
		assertTrue(initOccUntil != null);
		
		spotService.payMeter(testSpot, car, minToAdd);
		ZonedDateTime changedOccUntil = testSpot.getOccupyUntil();
		
		assertTrue(testSpot.getStatus() == Spot.SpotStatus.OCCUPIED);
		assertTrue(testSpot.getParker() == car.getOwner());
		assertTrue(changedOccUntil.minusMinutes(minToAdd).isEqual(initOccUntil));
	}
		
	@Test
	public void occupySpotShouldFailIfSpotStatusIsOccupiedByAnotherPerson() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);

		long minToAdd = 30;
		spotService.payMeter(testSpot, car, minToAdd);

		Car newCarFromAnotherUser = entityFactory.createCar();
		try {
			spotService.payMeter(testSpot, newCarFromAnotherUser, minToAdd);
			fail("Should not reach here");
		} catch (SpotOccupiedException ex) {

		}
	}

	@Test
	public void occupySpotShouldFailIfSpotStatusIsAvailableButNotUntilRequestedTime() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(1);
		spotService.makeSpotAvailable(testSpot, availUntil);

		try {
			spotService.payMeter(testSpot, car, 90);
			fail("Should not reach here");
		} catch (SpotUnavailableException ex) {
			logger.info(ex.getMessage());
		}
	}

	@Test
	public void occupySpotShouldPassUnderNormalCondition() {
		ZonedDateTime now = DateTimeUtil.now();
		ZonedDateTime availUntil = now.plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);

		long minToAdd = 30;
		spotService.payMeter(testSpot, car, minToAdd);

		assertTrue(testSpot.getStatus() == Spot.SpotStatus.OCCUPIED);
		assertTrue(testSpot.getAvailableUntil() == availUntil);
		assertTrue(testSpot.getParker() == car.getOwner());
		assertTrue(DateTimeUtil.almostEqual(testSpot.getOccupyUntil(), now.plusMinutes(minToAdd)));
		assertTrue(testSpot.getOccupyUntil().isBefore(testSpot.getAvailableUntil()));
	}

	/*
	 * 
	 * unoccupySpot
	 * 
	 * fail: If status is not occupied
	 * 
	 * pass: - Any other case
	 * 
	 */
	@Test
	public void unoccupySpotShouldFailWhenNotOccupied() {
		try {
			spotService.unoccupySpot(testSpot, entityFactory.createCar());
			fail("Should not reach here");
		} catch (SpotNotOccupiedException ex) {
		}

		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);
		try {
			spotService.unoccupySpot(testSpot, entityFactory.createCar());
			fail("Should not reach here");
		} catch (SpotNotOccupiedException ex) {
		}
	}

	@Test
	public void unoccupySpotShouldPassWhenOccupied() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);

		long minToAdd = 30;
		spotService.payMeter(testSpot, car, minToAdd);

		spotService.unoccupySpot(testSpot, car);

		assertTrue(testSpot.getStatus() == Spot.SpotStatus.AVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == availUntil);
		assertTrue(testSpot.getParker() == null);
		assertTrue(testSpot.getOccupyUntil() == null);
	}

	// clean up
	@Test
	public void cleanUpShouldCleanStaleSpotsAVAILABLE() {
		ZonedDateTime initAvailUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, initAvailUntil);

		testSpot.setAvailableUntil(DateTimeUtil.now().minusMinutes(1));
		assertTrue(testSpot.getStatus() == SpotStatus.AVAILABLE);
		assertTrue(testSpot.getAvailableUntil() != null);

		spotService.cleanUp();

		assertTrue(testSpot.getStatus() == SpotStatus.UNAVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == null);
	}

	@Test
	public void cleanUpShouldNotCleanIfSpotsNotStale() {
		ZonedDateTime availUntil = DateTimeUtil.now().plusHours(5);
		spotService.makeSpotAvailable(testSpot, availUntil);

		assertTrue(testSpot.getStatus() == SpotStatus.AVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == availUntil);

		int cleanedCount = spotService.cleanUp();

		assertTrue(testSpot.getStatus() == SpotStatus.AVAILABLE);
		assertTrue(testSpot.getAvailableUntil() == availUntil);
		assertTrue(cleanedCount == 0);

		spotService.payMeter(testSpot, car, 30);

		cleanedCount = spotService.cleanUp();

		assertTrue(testSpot.getStatus() == SpotStatus.OCCUPIED);
		assertTrue(testSpot.getAvailableUntil() != null);
		assertTrue(testSpot.getOccupyUntil() != null);
		assertTrue(testSpot.getParker() != null);
		assertTrue(cleanedCount == 0);
	}

	@Test
	public void submitSpotRegistrationFormShouldCreateNewSpotRegistrationFormAndSendEmail()
			throws FileNotFoundException, IOException {
		testUser = userService.save(testUser);
		SpotRegistrationForm form = new SpotRegistrationForm();
		form.setUserId(testUser.getId());
		form.setUserNote("I am registering a spot!");

		String path = "/Users/josephryu/Downloads/JosephRyu_Resume_2019.docx";
		File file = new File(path);
		MultipartFile multipartFile = new MockMultipartFile(file.getName(), new FileInputStream(file));
		form.getFiles().add(multipartFile);

		String path2 = "/Users/josephryu/Downloads/masks.jpeg";
		File file2 = new File(path2);
		MultipartFile multipartFile2 = new MockMultipartFile(file2.getName(), new FileInputStream(file2));
		form.getFiles().add(multipartFile2);

		spotService.receiveSpotRegistration(form);

		assertTrue(form.getId() != null);
		assertTrue(form.getStatus() == FormStatus.SUBMITTED_FOR_REVIEW);
		assertTrue(!form.getFiles().isEmpty());
	}

	@Test
	public void registerSpotShouldCreateNewSpotUnderUser() {
		testUser = userService.save(testUser);
		SpotRegistrationForm form = new SpotRegistrationForm();
		form.setUserId(testUser.getId());
		form.setUserNote("I am registering a spot!");
		Address address = new Address();
		address.setCity("Halifax");
		address.setCountry("Canada");
		form.setAddress(address);
		spotService.receiveSpotRegistration(form);

		spotService.approveSpot(form.getKey());
		assertTrue(form.getStatus() == FormStatus.APPROVED);
	}

	@Test
	public void rejectSpotShouldSetStatusToRejectedAndSendEmail() {
		testUser = userService.save(testUser);
		SpotRegistrationForm form = new SpotRegistrationForm();
		form.setUserId(testUser.getId());
		form.setUserNote("I am registering a spot!");
		Address address = new Address();
		address.setCity("Halifax");
		address.setCountry("Canada");
		form.setAddress(address);
		spotService.receiveSpotRegistration(form);

		spotService.rejectSpot(form.getKey(), "Reject reason");
		assertTrue(form.getStatus() == FormStatus.REJECTED);
	}

}
