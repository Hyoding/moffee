package com.ryusoftwareinc.moffee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.service.SpotService;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@Component
public class EntityFactory {
		
	@Autowired
	private UserService userService;
	
	@Autowired
	private SpotService spotService;
	
	public User createUser() {
		User testUser = new User();
		testUser.setFirstName("Joseph");
		testUser.setLastName("Ryu");
		testUser.setEmail("joseph.ryu1@gmail.com" + System.currentTimeMillis());
		testUser.setHashedPassword("pwd");
		testUser.setBalance(100);
		return userService.save(testUser);
	}
		
	public User createParker() {
		User parker = createUser();
		parker.setBalance(100);
		Car car = new Car();
		car.setOwner(parker);
		car.setLicensePlate("testLicensePlate");
		car.setModel("testModel");
		car.setColour("testColour");
		parker.add(car);
		return userService.save(parker);
	}
	
	public Car createCar() {
		User parker = createParker();
		Car car = parker.getCars().stream().findAny().get();
		return car;
	}
	
	public Spot createSpot(User user) {
		Spot spot = new Spot();
		spot.setOwner(user);
		spot.setRate(2.00);
		return spotService.save(spot);
	}

}
