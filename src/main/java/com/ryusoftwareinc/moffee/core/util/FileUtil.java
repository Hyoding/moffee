package com.ryusoftwareinc.moffee.core.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.ryusoftwareinc.moffee.config.Config;

@Component
public class FileUtil {
	
	private static final Logger logger = Logger.getLogger(FileUtil.class);
	
	@Autowired private Config config;
	
	public Collection<File> convertToFile(Collection<MultipartFile> mFiles) {
		List<File> files = new ArrayList<>();
		for (MultipartFile mFile : mFiles) {
			File file = new File(config.getTempPath() + File.separator + mFile.getOriginalFilename());
			try (OutputStream os = new FileOutputStream(file)) {
			    os.write(mFile.getBytes());
			    files.add(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return files;
	}

}
