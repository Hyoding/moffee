package com.ryusoftwareinc.moffee.core.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {
	
	private static final String DEFAULT_ZONE = "America/Halifax";
	private static final ZoneId ZONE_ID = ZoneId.of(DEFAULT_ZONE);
	
	private static final long ALMOST_EQUAL_CRITERIA_SEC = 5;
	
	private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private DateTimeUtil() {}
	
	public static ZonedDateTime now() {
		return ZonedDateTime.now(ZONE_ID);
	}
	
	public static String format(ZonedDateTime dateTime) {
		return dateTime.format(DEFAULT_FORMATTER);
	}
	
	public static int differenceInMin(ZonedDateTime dateTime1, ZonedDateTime dateTime2) {
		long diffInSec = dateTime1.toEpochSecond() - dateTime2.toEpochSecond();
		return (int) diffInSec / 60;
	}
	
	public static boolean almostEqual(ZonedDateTime dateTime1, ZonedDateTime dateTime2) {
		long diffInSec = differenceInMin(dateTime1, dateTime2);
		return diffInSec > -ALMOST_EQUAL_CRITERIA_SEC && diffInSec < ALMOST_EQUAL_CRITERIA_SEC;
	}
	
}
