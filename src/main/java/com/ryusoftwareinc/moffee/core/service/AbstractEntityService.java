package com.ryusoftwareinc.moffee.core.service;

import java.util.Optional;

import com.ryusoftwareinc.moffee.core.exception.EntityNotFoundException;
import com.ryusoftwareinc.moffee.core.model.AbstractEntity;
import com.ryusoftwareinc.moffee.core.repository.EntityRepository;

public abstract class AbstractEntityService<T extends AbstractEntity> implements EntityRepository<T> {
	
	public abstract EntityRepository<T> getRepo();

	@Override
	public <S extends T> S save(S entity) {
		return getRepo().save(entity);
	}

	@Override
	public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
		return getRepo().saveAll(entities);
	}

	@Override
	public Optional<T> findById(Long id) {
		return getRepo().findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return getRepo().existsById(id);
	}

	@Override
	public Iterable<T> findAll() {
		return getRepo().findAll();
	}

	@Override
	public Iterable<T> findAllById(Iterable<Long> ids) {
		return getRepo().findAllById(ids);
	}

	@Override
	public long count() {
		return getRepo().count();
	}

	@Override
	public void deleteById(Long id) {
		getRepo().deleteById(id);
	}

	@Override
	public void delete(T entity) {
		getRepo().delete(entity);
	}

	@Override
	public void deleteAll(Iterable<? extends T> entities) {
		getRepo().deleteAll(entities);
	}

	@Override
	public void deleteAll() {
		getRepo().deleteAll();
	}

	@Override
	public T findByKey(String key) {
		T entity = getRepo().findByKey(key);
		if (entity == null) {
			throw new EntityNotFoundException("Entity doens't exist " + key);
		}
		return entity;
	}
	
}
