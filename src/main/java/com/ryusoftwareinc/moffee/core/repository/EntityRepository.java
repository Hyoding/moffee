package com.ryusoftwareinc.moffee.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.ryusoftwareinc.moffee.core.model.AbstractEntity;

@NoRepositoryBean
public interface EntityRepository<T extends AbstractEntity> extends CrudRepository<T, Long> {
	
	T findByKey(String key);

}