package com.ryusoftwareinc.moffee.core.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
	
	private String streetAddress;
	private String unit;
	private String city;
	private String province;
	private String country;
	private String postalCode;
	
	public Address() {
		this.streetAddress = "";
		this.unit = "";
		this.city = "";
		this.country = "";
		this.postalCode = "";
		this.province = "";
	}
	
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
}
