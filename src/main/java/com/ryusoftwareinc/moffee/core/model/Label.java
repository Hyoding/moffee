package com.ryusoftwareinc.moffee.core.model;

public interface Label {

	String getLabel();
	
}
