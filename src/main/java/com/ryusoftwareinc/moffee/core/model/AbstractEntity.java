package com.ryusoftwareinc.moffee.core.model;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.moffee.core.EntityTrigger;
import com.ryusoftwareinc.moffee.core.json.Jsonify;
import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.core.validator.ErrorCollector;
import com.ryusoftwareinc.moffee.core.validator.Validator;

@EntityListeners(EntityTrigger.class)
@MappedSuperclass
public abstract class AbstractEntity implements Validator, Jsonify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    protected String key;
    @JsonIgnore
    protected ZonedDateTime createdDate;
    @JsonIgnore
    protected ZonedDateTime lastModifiedDate;
    protected boolean enabled;

    public AbstractEntity() {
        this.id = null;
        this.key = UUID.randomUUID().toString();
        this.createdDate = DateTimeUtil.now();
        this.lastModifiedDate = null;
        this.enabled = true;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    
    public String getKey() { return key; }

    public ZonedDateTime getCreatedDate() { return createdDate; }
    public void setCreatedDate(ZonedDateTime createdDate) { this.createdDate = createdDate; }

    public ZonedDateTime getLastModifiedDate() { return lastModifiedDate; }
    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) { this.lastModifiedDate = lastModifiedDate; }

    public boolean isEnabled() { return enabled; }
	public void setEnabled(boolean enabled) { this.enabled = enabled; }

	@Override
    public final boolean equals(Object other) {
        if (!(other instanceof AbstractEntity)) {
            return false;
        }
        AbstractEntity otherEntity = (AbstractEntity) other;
        return this.key.equals(otherEntity.getKey());
    }
	
	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	@Override
	public ErrorCollector collectErrors() {
		return new ErrorCollector();
	}
	
	@Override
	public String toString() {
		return toJSON();
	}

}