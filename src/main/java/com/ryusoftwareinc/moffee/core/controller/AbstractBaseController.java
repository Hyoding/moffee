package com.ryusoftwareinc.moffee.core.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ryusoftwareinc.moffee.core.exception.ClientException;
import com.ryusoftwareinc.moffee.core.exception.Concerning;
import com.ryusoftwareinc.moffee.core.http.HttpResponse;
import com.ryusoftwareinc.moffee.notification.service.NotificationService;

@Component
public abstract class AbstractBaseController {
	
	private static final Logger logger = Logger.getLogger(AbstractBaseController.class);
	
	private static final HttpStatus CLIENT_ERROR_CODE = HttpStatus.BAD_REQUEST;
	private static final HttpStatus SERVER_ERROR_CODE = HttpStatus.INTERNAL_SERVER_ERROR;
		
	@Autowired private NotificationService notificationService;
	
	protected HttpResponse formResponse(Object data) {
		return formResponse(data, null);
	}
	
	protected HttpResponse formResponse(Object data, String msg) {
		HttpResponse response = new HttpResponse();
		response.setResponseData(data);
		response.setMessage(msg);
		return response;
	}
	
	/*
	 * Think about how many status we want to use. 
	 */
	@ExceptionHandler({ RuntimeException.class})
    public ResponseEntity<HttpResponse> handleException(RuntimeException ex) {
		logger.info("Handling exception: " + ex.getClass().getSimpleName());

		if (ex instanceof DataIntegrityViolationException) {
			notificationService.sendErrorToDev(ex);
			return new ResponseEntity<>(formResponse(null, "Unable to perform this DB operation"), CLIENT_ERROR_CODE);
		}
		
		if (ex instanceof ClientException) {
			if (ex instanceof Concerning) {
				notificationService.sendErrorToDev(ex);
			}
			return new ResponseEntity<>(formResponse(null, ex.getMessage()), CLIENT_ERROR_CODE);
		}
		
//		if (ex instanceof ServerException) {
//			return new ResponseEntity<>(formResponse(null, "Server error. If problem continues please contact the developer"), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
		
		notificationService.sendErrorToDev(ex);
		return new ResponseEntity<>(formResponse(null, "Server error. If problem continues please contact the developer"), SERVER_ERROR_CODE);
    }

}
