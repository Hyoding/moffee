package com.ryusoftwareinc.moffee.core.validator;

import com.ryusoftwareinc.moffee.core.exception.ClientException;

public class ValidationException extends ClientException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public ValidationException(String msg) {
		super(msg);
	}

}
