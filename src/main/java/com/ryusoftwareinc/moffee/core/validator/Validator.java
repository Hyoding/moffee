package com.ryusoftwareinc.moffee.core.validator;

public interface Validator {

	ErrorCollector collectErrors();
	
	default boolean isValid() {
		return collectErrors().isEmpty();
	}
	
}
