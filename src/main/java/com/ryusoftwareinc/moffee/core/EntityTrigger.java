package com.ryusoftwareinc.moffee.core;

import java.time.ZonedDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.ryusoftwareinc.moffee.core.model.AbstractEntity;

public class EntityTrigger {
	
	@PrePersist
    @PreUpdate
    private void beforeInsertOrUpdate(AbstractEntity entity) {
		if (entity.getId() != null) {
			entity.setLastModifiedDate(ZonedDateTime.now());
		}
		entity.collectErrors().boom();
	}
	
}
