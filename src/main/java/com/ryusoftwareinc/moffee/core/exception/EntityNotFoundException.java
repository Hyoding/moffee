package com.ryusoftwareinc.moffee.core.exception;

public class EntityNotFoundException extends ClientException implements Concerning {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public EntityNotFoundException(String msg) {
		super(msg);
	}

}
