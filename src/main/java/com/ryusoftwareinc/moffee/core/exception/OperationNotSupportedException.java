package com.ryusoftwareinc.moffee.core.exception;

public class OperationNotSupportedException extends ServerException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public OperationNotSupportedException(String msg) {
		super(msg);
	}

}
