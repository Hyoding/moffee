package com.ryusoftwareinc.moffee.core.http;

import com.ryusoftwareinc.moffee.core.json.Jsonify;

public class BaseHttpRequest implements Jsonify {

	private HttpRequester requester;

	public HttpRequester getRequester() {
		return requester;
	}

	public void setRequester(HttpRequester requester) {
		this.requester = requester;
	}

	@Override
	public String toString() {
		return toJSON();
	}

}
