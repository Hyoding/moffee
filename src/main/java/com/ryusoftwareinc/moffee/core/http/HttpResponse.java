package com.ryusoftwareinc.moffee.core.http;

import com.ryusoftwareinc.moffee.core.json.Jsonify;

public class HttpResponse implements Jsonify {
	
	private String message;
	private Object responseData;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResponseData() {
		return responseData;
	}
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}
	
	@Override
	public String toString() {
		return toJSON();
	}

}
