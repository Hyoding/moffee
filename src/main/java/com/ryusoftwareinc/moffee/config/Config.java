package com.ryusoftwareinc.moffee.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*
 * Later retrieve all from db
 */
@Component
public class Config {

	@Value("${url.base}")
	private String urlBase;

	@Value("${url.spot.approveSpot}")
	private String urlSpotApproveSpot;

	@Value("${url.spot.rejectSpot}")
	private String urlSpotRejectSpot;

	@Value("${file.temp.path}")
	private String tempPath;

	@Value("${amt.spot.cutPercentage}")
	private String amtSpotCutPercentage;

	@Value("${amt.service.cutPercentage}")
	private String amtServiceCutPercentage;

	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	public String getUrlSpotApproveSpot() {
		return urlSpotApproveSpot;
	}

	public void setUrlSpotApproveSpot(String urlUserApproveSpot) {
		this.urlSpotApproveSpot = urlUserApproveSpot;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getUrlSpotRejectSpot() {
		return urlSpotRejectSpot;
	}

	public void setUrlSpotRejectSpot(String urlUserRejectSpot) {
		this.urlSpotRejectSpot = urlUserRejectSpot;
	}

	public String getAmtSpotCutPercentage() {
		return amtSpotCutPercentage;
	}

	public void setAmtSpotCutPercentage(String amtSpotCutPercentage) {
		this.amtSpotCutPercentage = amtSpotCutPercentage;
	}

	public String getAmtServiceCutPercentage() {
		return amtServiceCutPercentage;
	}

	public void setAmtServiceCutPercentage(String amtServiceCutPercentage) {
		this.amtServiceCutPercentage = amtServiceCutPercentage;
	}

}
