package com.ryusoftwareinc.moffee.config;

public enum Environment {
	DEV, PROD
}
