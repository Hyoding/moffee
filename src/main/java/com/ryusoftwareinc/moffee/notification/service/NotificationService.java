package com.ryusoftwareinc.moffee.notification.service;

import java.io.File;
import java.time.ZonedDateTime;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.config.Environment;
import com.ryusoftwareinc.moffee.notification.exception.EmailException;
import com.ryusoftwareinc.moffee.notification.model.Email;

@Service
@Transactional
public class NotificationService {
	
	private static final Logger logger = Logger.getLogger(NotificationService.class);
	
	@Autowired
    private JavaMailSender emailSender;
	
	@Value("${app.environment}")
	private Environment env;
	
	@Value("${spring.mail.username}")
	private String emailFrom;
		
	@Async
	public void sendEmail(Email email) {
		if (env == Environment.DEV) {
			logger.info(email);
			return;
		}
		
		MimeMessage message = emailSender.createMimeMessage();
	    try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(email.getFrom());
			helper.setTo(email.getTo().toArray(new String[email.getTo().size()]));
			helper.setCc(email.getCc().toArray(new String[email.getCc().size()]));
			helper.setBcc(email.getBcc().toArray(new String[email.getBcc().size()]));
			helper.setSubject(email.getSubject());
			helper.setText(email.getText());
			for (File file : email.getAttachments()) {
				helper.addAttachment(file.getName(), file);
			}
			emailSender.send(message);
		} catch (MessagingException e) {
			logger.error(e);
			throw new EmailException("Error when sending email");
		}
	}
	
	@Async
	public void sendErrorToDev(Exception ex) {
		StringBuilder sb = new StringBuilder();
		sb.append("Time: " + ZonedDateTime.now());
		sb.append("\n");
		sb.append("Thread: " + Thread.currentThread().getName());
		sb.append("\n\n");
		sb.append(ExceptionUtils.getStackTrace(ex));
		
		Email email = new Email();
		email.setFrom(emailFrom);
		email.addTo(emailFrom);
		email.setSubject("Runtime Error caught");
		email.setText(sb.toString());
		sendEmail(email);
	}

}
