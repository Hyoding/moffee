package com.ryusoftwareinc.moffee.notification.model;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Email {

	private String subject;
	private String text;
	private String from;
	private Set<String> to;
	private Set<String> cc;
	private Set<String> bcc;
	private Set<File> attachments;
	
	public Email() {
		this.subject = "";
		this.text = "";
		this.from = "";
		this.to = new HashSet<>();
		this.cc = new HashSet<>();
		this.bcc = new HashSet<>();
		this.attachments = new HashSet<>();
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public Set<String> getTo() {
		return to;
	}
	public void setTo(Set<String> to) {
		this.to = to;
	}
	public void addTo(String to) {
		this.to.add(to);
	}
	public Set<String> getCc() {
		return cc;
	}
	public void addCc(String to) {
		this.cc.add(to);
	}
	public void setCc(Set<String> cc) {
		this.cc = cc;
	}
	public Set<String> getBcc() {
		return bcc;
	}
	public void setBcc(Set<String> bcc) {
		this.bcc = bcc;
	}
	public void addBcc(String to) {
		this.bcc.add(to);
	}
	public Set<File> getAttachments() {
		return attachments;
	}
	public void setAttachments(Set<File> attachments) {
		this.attachments = attachments;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("Subject: " + subject);
		sb.append("\n");
		sb.append("From: " + from);
		sb.append("\n");
		sb.append("To: " + String.join(", ", to));
		sb.append("\n");
		sb.append("cc: " + String.join(", ", cc));
		sb.append("\n");
		sb.append("bcc: " + String.join(", ", bcc));
		sb.append("\n\n");
		sb.append(text);
		sb.append("\n\n");
		sb.append("Attachment: " + String.join(", ", attachments.stream().map(f -> f.getName()).collect(Collectors.toList())));
		return sb.toString();
	}
	
}
