package com.ryusoftwareinc.moffee.notification.exception;

import com.ryusoftwareinc.moffee.core.exception.ServerException;

public class EmailException extends ServerException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public EmailException(String msg) {
		super(msg);
	}

}
