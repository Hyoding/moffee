package com.ryusoftwareinc.moffee.spot.repository;

import java.util.Collection;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.Spot.SpotStatus;

public interface SpotRepository extends EntityRepository<Spot> {

	Collection<Spot> findByStatusNot(SpotStatus status);
	
}
