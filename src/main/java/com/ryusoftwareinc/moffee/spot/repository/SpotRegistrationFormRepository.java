package com.ryusoftwareinc.moffee.spot.repository;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationForm;

public interface SpotRegistrationFormRepository extends EntityRepository<SpotRegistrationForm> {
	
}
