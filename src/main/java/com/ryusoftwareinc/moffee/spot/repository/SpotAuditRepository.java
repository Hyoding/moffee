package com.ryusoftwareinc.moffee.spot.repository;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.SpotAudit;

public interface SpotAuditRepository extends EntityRepository<SpotAudit> {
	
	Iterable<SpotAudit> findAllBySpot(Spot spot);
	
}
