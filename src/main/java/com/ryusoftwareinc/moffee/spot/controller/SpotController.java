package com.ryusoftwareinc.moffee.spot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.car.service.CarService;
import com.ryusoftwareinc.moffee.core.controller.AbstractBaseController;
import com.ryusoftwareinc.moffee.core.exception.OperationNotSupportedException;
import com.ryusoftwareinc.moffee.core.http.HttpResponse;
import com.ryusoftwareinc.moffee.core.model.Address;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.SpotOperationRequest;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationForm;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationFormDto;
import com.ryusoftwareinc.moffee.spot.service.SpotService;
import com.ryusoftwareinc.moffee.spot.service.SpotService.SpotOperation;

@RequestMapping("/spot")
@RestController
@CrossOrigin
public class SpotController extends AbstractBaseController {

	@Autowired
	private SpotService spotService;
	
	@Autowired
	private CarService carService;
		
	@PostMapping(value = "/submitRegistration", consumes = {"multipart/form-data"})
	public HttpResponse submitSpotRegistration(@ModelAttribute SpotRegistrationFormDto formDto) {
		SpotRegistrationForm form = new SpotRegistrationForm();
		form.setUserId(formDto.getUserId());
		form.setUserNote(formDto.getNote());
		form.setFiles(formDto.getFiles());
		Address address = new Address();
		address.setStreetAddress(formDto.getStreetAddress());
		address.setUnit(formDto.getUnit());
		address.setCity(formDto.getCity());
		address.setProvince(formDto.getProvince());
		address.setCountry(formDto.getCountry());
		address.setPostalCode(formDto.getPostalCode());
		form.setAddress(address);
		
		spotService.receiveSpotRegistration(form);
		return formResponse(null, "Spot registration submitted");
	}

	@GetMapping("/approve")
	public HttpResponse approveSpot(@RequestParam String formKey) {
		spotService.approveSpot(formKey);
		return formResponse(null, "Spot registration approved for " + formKey);
	}

	@GetMapping("/reject")
	public HttpResponse rejectSpot(@RequestParam String formKey, @RequestParam(required = false) String reason) {
		if (reason == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("Documents were missing. Please send clear picture of below:\n\n");
			sb.append("  -  Government issued ID with address\n");
			sb.append("  -  Photo of actual parking spot you want to register\n");
			reason = sb.toString();
		}
		spotService.rejectSpot(formKey, reason);
		return formResponse(null, "Spot Registration rejected for " + formKey);
	}

	@GetMapping("/remove")
	public HttpResponse removeSpot(@RequestParam String formKey) {
		spotService.removeSpot(formKey);
		return formResponse(null, "Spot removed for " + formKey);
	}
	
	@PostMapping("/spotOperation")
	public HttpResponse spotOperation(@RequestBody SpotOperationRequest request) {
//		SpotOperation operation = SpotOperation.valueOf(request.getSpotOperation());
		SpotOperation operation = request.getSpotOperation();
		Spot spot = spotService.findByKey(request.getSpotKey());
		switch (operation) {
		case MAKE_SPOT_AVAILABLE:
			Double newRate = request.getNewRate();
			if (newRate != null) {
				spot.setRate(newRate);
			}
			spotService.makeSpotAvailable(spot, request.getNewAvailableUntil());
			break;
		case MAKE_SPOT_UNAVAILABLE:
			spotService.makeSpotUnavailable(spot);
			break;
		case PAY_METER:
			Car car = carService.findByKey(request.getCarKey());
			long additionalMin = request.getPayDurInMin();
			spotService.payMeter(spot, car, additionalMin);
			break;
		case UNOCCUPY_SPOT:
//			car = carService.findByKey(request.getCarKey());
//			spotService.unoccupySpot(spot, car);
//			break;
		default:
			throw new OperationNotSupportedException("This spot operation not supported: " + operation);
		}
		return formResponse(spot, "Spot availability changed");
	}

}
