package com.ryusoftwareinc.moffee.spot.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import com.ryusoftwareinc.moffee.core.model.AbstractFormEntity;
import com.ryusoftwareinc.moffee.core.model.Address;

@Entity
public class SpotRegistrationForm extends AbstractFormEntity {
	
	@Embedded
	private Address address;
	
	public SpotRegistrationForm() {
		this.address = new Address();
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
