package com.ryusoftwareinc.moffee.spot.model;

import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.ryusoftwareinc.moffee.core.model.AbstractEntity;
import com.ryusoftwareinc.moffee.spot.model.Spot.SpotStatus;

@Entity
public class SpotAudit extends AbstractEntity {
	
	@ManyToOne
	private Spot spot;
	
	private Double rate;
	@Enumerated(EnumType.STRING)
	private SpotStatus status;
	private ZonedDateTime availableUntil;
	private Long parkerId;
	private ZonedDateTime occupiedUntil;
	
	public SpotAudit() {}
	
	public SpotAudit(Spot spot) {
		this.spot = spot;
		this.rate = spot.getRate();
		this.status = spot.getStatus();
		this.availableUntil = spot.getAvailableUntil();
		this.parkerId = spot.getParker() != null ? spot.getParker().getId() : null;
		this.occupiedUntil = spot.getOccupyUntil();
	}
	
	public Spot getSpot() {
		return spot;
	}
	public void setSpot(Spot spot) {
		this.spot = spot;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public SpotStatus getStatus() {
		return status;
	}
	public void setStatus(SpotStatus status) {
		this.status = status;
	}
	public ZonedDateTime getAvailableUntil() {
		return availableUntil;
	}
	public void setAvailableUntil(ZonedDateTime availableUntil) {
		this.availableUntil = availableUntil;
	}
	public Long getParkerId() {
		return parkerId;
	}
	public void setParkerId(Long parkerId) {
		this.parkerId = parkerId;
	}
	public ZonedDateTime getOccupiedUntil() {
		return occupiedUntil;
	}
	public void setOccupiedUntil(ZonedDateTime occupiedUntil) {
		this.occupiedUntil = occupiedUntil;
	}

}
