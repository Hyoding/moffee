package com.ryusoftwareinc.moffee.spot.model;

import java.time.ZonedDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.ryusoftwareinc.moffee.core.http.BaseHttpRequest;
import com.ryusoftwareinc.moffee.spot.service.SpotService.SpotOperation;

public class SpotOperationRequest extends BaseHttpRequest {

	private String spotKey;
	private SpotOperation spotOperation;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime newAvailableUntil;
	private Double newRate;
	private String newParkerKey;
	private String carKey;
	private long payDurInMin;

	public String getSpotKey() {
		return spotKey;
	}

	public void setSpotKey(String spotKey) {
		this.spotKey = spotKey;
	}

	public SpotOperation getSpotOperation() {
		return spotOperation;
	}

	public void setSpotOperation(SpotOperation spotOperation) {
		this.spotOperation = spotOperation;
	}

	public ZonedDateTime getNewAvailableUntil() {
		return newAvailableUntil;
	}

	public void setNewAvailableUntil(ZonedDateTime newAvailableUntil) {
		this.newAvailableUntil = newAvailableUntil;
	}

	public String getNewParkerKey() {
		return newParkerKey;
	}

	public void setNewParkerKey(String newParkerKey) {
		this.newParkerKey = newParkerKey;
	}

	public long getPayDurInMin() {
		return payDurInMin;
	}

	public void setPayDurInMin(long payDurInMin) {
		this.payDurInMin = payDurInMin;
	}

	public String getCarKey() {
		return carKey;
	}

	public void setCarKey(String carKey) {
		this.carKey = carKey;
	}

	public Double getNewRate() {
		return newRate;
	}

	public void setNewRate(Double newRate) {
		this.newRate = newRate;
	}

}
