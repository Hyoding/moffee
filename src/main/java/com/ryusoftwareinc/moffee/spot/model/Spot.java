package com.ryusoftwareinc.moffee.spot.model;

import java.time.ZonedDateTime;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.moffee.core.model.AbstractEntity;
import com.ryusoftwareinc.moffee.core.model.Address;
import com.ryusoftwareinc.moffee.user.model.ParkerDto;
import com.ryusoftwareinc.moffee.user.model.User;

@Entity
public class Spot extends AbstractEntity {
	
	public enum SpotStatus {
		UNAVAILABLE,
		AVAILABLE,
		OCCUPIED
	}
	
	private static final SpotStatus DEFAULT_STATUS = SpotStatus.UNAVAILABLE;
	
	@OneToOne
	@JsonIgnore
	private User owner;
	
	@Embedded
	private Address address;
	private Double rate;
	
	@Enumerated(EnumType.STRING)
	private SpotStatus status;
	private ZonedDateTime availableUntil;
	@OneToOne
	@JsonIgnore
	private User parker;
	private ZonedDateTime occupyUntil;
	
	@Transient
	private ParkerDto parkerDto;
	
	public Spot() {
		this.address = new Address();
		this.rate = 0.0;
		this.status = DEFAULT_STATUS;
		this.availableUntil = null;
		this.parker = null;
		this.occupyUntil = null;
	}
	
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}	

	public SpotStatus getStatus() {
		return status;
	}

	public void setStatus(SpotStatus status) {
		this.status = status;
	}

	public ZonedDateTime getAvailableUntil() {
		return availableUntil;
	}

	public void setAvailableUntil(ZonedDateTime availableUntil) {
		this.availableUntil = availableUntil;
	}

	public User getParker() {
		return parker;
	}

	public void setParker(User parker) {
		this.parker = parker;
	}

	public ZonedDateTime getOccupyUntil() {
		return occupyUntil;
	}

	public void setOccupyUntil(ZonedDateTime occupiedUntil) {
		this.occupyUntil = occupiedUntil;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public ParkerDto getParkerDto() {
		return parkerDto;
	}

	public void setParkerDto(ParkerDto parkerDto) {
		this.parkerDto = parkerDto;
	}
	
}
