package com.ryusoftwareinc.moffee.spot.exception;

import com.ryusoftwareinc.moffee.core.exception.ClientException;

public class SpotUnavailableException extends ClientException {

	private static final long serialVersionUID = -8436039137344787092L;

	public SpotUnavailableException(String msg) {
		super(msg);
	}

}
