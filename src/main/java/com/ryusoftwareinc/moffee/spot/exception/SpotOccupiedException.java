package com.ryusoftwareinc.moffee.spot.exception;

import com.ryusoftwareinc.moffee.core.exception.ClientException;

public class SpotOccupiedException extends ClientException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public SpotOccupiedException(String msg) {
		super(msg);
	}

}
