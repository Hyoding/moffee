package com.ryusoftwareinc.moffee.spot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.SpotAudit;
import com.ryusoftwareinc.moffee.spot.repository.SpotAuditRepository;

@Service
@Transactional
public class SpotAuditService extends AbstractEntityService<SpotAudit> {
	
	@Autowired
	private SpotAuditRepository spotAuditRepo;

	@Override
	public EntityRepository<SpotAudit> getRepo() {
		return spotAuditRepo;
	}
	
	public SpotAudit audit(Spot spot) {
		SpotAudit audit = new SpotAudit(spot);
		return spotAuditRepo.save(audit);
	}
	
	public Iterable<SpotAudit> findAllBySpot(Spot spot) {
		return spotAuditRepo.findAllBySpot(spot);
	}

}
