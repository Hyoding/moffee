package com.ryusoftwareinc.moffee.spot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationForm;
import com.ryusoftwareinc.moffee.spot.repository.SpotRegistrationFormRepository;

@Service
@Transactional
public class SpotRegistrationFormService extends AbstractEntityService<SpotRegistrationForm> {
	
	@Autowired
	private SpotRegistrationFormRepository spotRegFormRepo;

	@Override
	public EntityRepository<SpotRegistrationForm> getRepo() {
		return spotRegFormRepo;
	}

}
