package com.ryusoftwareinc.moffee.spot.service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.config.Config;
import com.ryusoftwareinc.moffee.core.exception.ClientException;
import com.ryusoftwareinc.moffee.core.exception.ServerException;
import com.ryusoftwareinc.moffee.core.model.AbstractFormEntity.FormStatus;
import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.core.util.FileUtil;
import com.ryusoftwareinc.moffee.invoice.InvoiceService;
import com.ryusoftwareinc.moffee.notification.model.Email;
import com.ryusoftwareinc.moffee.notification.service.NotificationService;
import com.ryusoftwareinc.moffee.spot.exception.SpotNotOccupiedException;
import com.ryusoftwareinc.moffee.spot.exception.SpotOccupiedException;
import com.ryusoftwareinc.moffee.spot.exception.SpotRegisgrationAlreadyProcessedException;
import com.ryusoftwareinc.moffee.spot.exception.SpotUnavailableException;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.spot.model.Spot.SpotStatus;
import com.ryusoftwareinc.moffee.spot.model.SpotRegistrationForm;
import com.ryusoftwareinc.moffee.spot.repository.SpotRepository;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@Service
@Transactional
public class SpotService extends AbstractEntityService<Spot> {

	private static Logger logger = Logger.getLogger(SpotService.class);

	private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private SpotRepository spotRepo;

	@Autowired
	private UserService userService;

	@Autowired
	private SpotAuditService auditService;

	@Autowired
	private SpotRegistrationFormService spotRegistrationFormService;

	@Value("${spring.mail.username}")
	private String emailFrom;

	@Autowired
	private NotificationService notifService;

	@Autowired
	private FileUtil fileUtil;

	@Autowired
	private Config config;

	@Autowired
	private InvoiceService invoiceService;

	@Override
	public EntityRepository<Spot> getRepo() {
		return spotRepo;
	}

	public void makeSpotAvailable(Spot spot, ZonedDateTime availableUntil) {
		if (spot == null || availableUntil == null) {
			throw new ClientException("Please provide all information");
		}
		if (spot.getStatus() == SpotStatus.OCCUPIED && availableUntil.isBefore(spot.getOccupyUntil())) {
			throw new SpotOccupiedException(
					"Spot is currently occupied until: " + spot.getOccupyUntil().format(DEFAULT_FORMATTER));
		}

		spot.setStatus(SpotStatus.AVAILABLE);
		spot.setAvailableUntil(availableUntil);
		auditService.audit(spot);
	}

	public void makeSpotUnavailable(Spot spot) {
		if (spot.getStatus() == SpotStatus.OCCUPIED) {
			throw new SpotOccupiedException(
					"Spot is currently occupied until: " + spot.getOccupyUntil().format(DEFAULT_FORMATTER));
		}
		spot.setStatus(SpotStatus.UNAVAILABLE);
		spot.setAvailableUntil(null);
		spot.setOccupyUntil(null);
		spot.setParker(null);
		auditService.audit(spot);
	}
	
	public void payMeter(Spot spot, Car car, long minToAdd) {
		if (spot.getStatus() == SpotStatus.UNAVAILABLE) {
			throw new SpotUnavailableException("This spot is unavailable");
		}

		User parker = car.getOwner();
		ZonedDateTime availUntil = spot.getAvailableUntil();
		ZonedDateTime newOccupyUntil = null;
		if (spot.getStatus() == SpotStatus.AVAILABLE) {
			newOccupyUntil = DateTimeUtil.now().plusMinutes(minToAdd);
		} else if (spot.getStatus() == SpotStatus.OCCUPIED) {
			newOccupyUntil = spot.getOccupyUntil().plusMinutes(minToAdd);
			if (!parker.equals(spot.getParker())) {
				throw new SpotOccupiedException("This spot is already taken");
			}
		} else {
			throw new ServerException("Unrecognized SpotStatus: " + spot.getStatus());
		}

		if (newOccupyUntil.isAfter(availUntil)) {
			throw new SpotUnavailableException(
					"This spot is only available until: " + DateTimeUtil.format(spot.getAvailableUntil()));
		}

		spot.setStatus(SpotStatus.OCCUPIED);
		spot.setOccupyUntil(newOccupyUntil);
		spot.setParker(parker);
		auditService.audit(spot);

		invoiceService.chargeTime(spot, minToAdd);
	}
	
	public void unoccupySpot(Spot spot) {
		if (spot.getStatus() != SpotStatus.OCCUPIED) {
			throw new SpotNotOccupiedException("This spot is not occupied");
		}
		spot.setStatus(SpotStatus.AVAILABLE);
		spot.setOccupyUntil(null);
		spot.setParker(null);
		auditService.audit(spot);
		
		invoiceService.closeInvoice(spot);
	}

	// this to be used later when users are actually unoccupying spots early to retrieve money back
	public void unoccupySpot(Spot spot, Car car) {
		if (spot.getStatus() != SpotStatus.OCCUPIED) {
			throw new SpotNotOccupiedException("This spot is not occupied");
		}
		if (!spot.getParker().has(car)) {
			throw new ServerException("Car doesn't belong to the parker. Car key: " + car.getKey());
		}
		spot.setStatus(SpotStatus.AVAILABLE);
		spot.setOccupyUntil(null);
		spot.setParker(null);
		auditService.audit(spot);
		
		invoiceService.closeInvoice(spot);
	}

	/*
	 * TODO: only fetch status unavailable AND getAvail/occpiedUntil is after now
	 */
	@Scheduled(fixedRate = 1000 * 60, initialDelayString = "60000")
	public int cleanUp() {
		logger.info("Cleaning..");
		ZonedDateTime now = DateTimeUtil.now();
		int makeSpotUnavilableCount = 0;
		int unoccupySpotCount = 0;

		try {
			for (Spot spot : spotRepo.findByStatusNot(SpotStatus.UNAVAILABLE)) {
				if (spot.getAvailableUntil().isEqual(now) || spot.getAvailableUntil().isBefore(now)) {
					makeSpotUnavailable(spot);
					++makeSpotUnavilableCount;
				} else if (spot.getStatus() == SpotStatus.OCCUPIED) {
					if (spot.getOccupyUntil().isEqual(now) || spot.getOccupyUntil().isBefore(now)) {
						unoccupySpot(spot);
						++unoccupySpotCount;
					}
				}
			}
		} catch (Exception ex) {
			notifService.sendErrorToDev(ex);
		}

		logger.info("makeSpotUnavilableCount: " + makeSpotUnavilableCount);
		logger.info("unoccupySpotCount: " + unoccupySpotCount);
		return makeSpotUnavilableCount + unoccupySpotCount;
	}

	public void receiveSpotRegistration(SpotRegistrationForm form) {
		form = spotRegistrationFormService.save(form);
		Email devEmail = generateEmailReviewSpotForm(form);
		notifService.sendEmail(devEmail);
	}

	public void approveSpot(String formKey) {
		SpotRegistrationForm form = spotRegistrationFormService.findByKey(formKey);
		if (form.getStatus() != FormStatus.SUBMITTED_FOR_REVIEW) {
			throw new SpotRegisgrationAlreadyProcessedException("Spot Registration already processed for " + formKey);
		}
		form.setStatus(FormStatus.APPROVED);

		User user = userService.findById(form.getUserId()).get();
		Spot newSpot = new Spot();
		newSpot.setOwner(user);
		newSpot.setAddress(form.getAddress());
		user.getSpots().add(newSpot);

		Email userEmail = generateEmailApproveSpot(form);
		notifService.sendEmail(userEmail);
	}

	public void rejectSpot(String formKey, String reason) {
		SpotRegistrationForm form = spotRegistrationFormService.findByKey(formKey);
		if (form.getStatus() != FormStatus.SUBMITTED_FOR_REVIEW) {
			throw new SpotRegisgrationAlreadyProcessedException("Spot Registration already processed for " + formKey);
		}
		form.setStatus(FormStatus.REJECTED);
		form.setProcessorNote(reason);
		Email rejectEmail = generateEmailRejectSpot(form);
		notifService.sendEmail(rejectEmail);
	}

	public void removeSpot(String spotKey) {
		Spot spot = findByKey(spotKey);
		spot.setEnabled(true);
	}

	private Email generateEmailApproveSpot(SpotRegistrationForm form) {
		User user = userService.findById(form.getUserId()).get();
		Email email = new Email();
		email.setSubject("Spot Registration Processed");
		email.setFrom(emailFrom);
		email.addTo(user.getEmail());
		email.addBcc(emailFrom);

		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + user.getFirstName() + ",\n\n");
		sb.append("Spot approved for address: \n\n" + form.getAddress());
		sb.append("\n\n");
		sb.append("Start earning money now by making your spot available!");
		email.setText(sb.toString());

		return email;
	}

	private Email generateEmailRejectSpot(SpotRegistrationForm form) {
		User user = userService.findById(form.getUserId()).get();

		Email email = new Email();
		email.setSubject("Spot Registration Rejected for form: " + form.getKey());
		email.setFrom(emailFrom);
		email.getTo().add(user.getEmail());
		email.getBcc().add(emailFrom);

		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + user.getFirstName() + ",\n\n");
		sb.append("Spot rejected for address: \n\n" + form.getAddress());
		sb.append("\n\n");
		sb.append("Processor comment: " + form.getProcessorNote());
		email.setText(sb.toString());

		return email;
	}

	private Email generateEmailReviewSpotForm(SpotRegistrationForm form) {
		Email email = new Email();
		email.setSubject("Spot Registration Form for user: " + form.getKey());
		email.setFrom(emailFrom);
		email.getTo().add(emailFrom);

		StringBuilder sb = new StringBuilder();
		sb.append("form key: " + form.getKey());
		sb.append("\n\n");
		sb.append(form.getUserNote());
		sb.append("\n\n");
		sb.append("URL to approve: " + config.getUrlBase() + config.getUrlSpotApproveSpot() + "?formKey="
				+ form.getKey());
		sb.append("\n\n");
		sb.append("URL to reject with default msg: " + config.getUrlBase() + config.getUrlSpotRejectSpot() + "?formKey="
				+ form.getKey());
		email.setText(sb.toString());

		email.setAttachments(new HashSet<>(fileUtil.convertToFile(form.getFiles())));
		return email;
	}

	public enum SpotOperation {
		MAKE_SPOT_AVAILABLE, MAKE_SPOT_UNAVAILABLE, PAY_METER,
		UNOCCUPY_SPOT
	}

}
