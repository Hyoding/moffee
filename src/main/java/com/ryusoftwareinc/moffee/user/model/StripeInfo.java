package com.ryusoftwareinc.moffee.user.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.moffee.core.model.AbstractEntity;

@Entity
public class StripeInfo extends AbstractEntity {
	
	@OneToOne
	@JsonIgnore
	private User user;
	
	private String customerId; // has CC information
	private String accountId; // has banking information
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
}
