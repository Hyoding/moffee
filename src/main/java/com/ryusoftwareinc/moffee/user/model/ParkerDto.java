package com.ryusoftwareinc.moffee.user.model;

public class ParkerDto {

	private String firstName;
	private String lastName;
	private String email;
	
	public ParkerDto() {}
	
	public ParkerDto(User parker) {
		this.firstName = parker.getFirstName();
		this.lastName = parker.getLastName();
		this.email = parker.getEmail();
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
