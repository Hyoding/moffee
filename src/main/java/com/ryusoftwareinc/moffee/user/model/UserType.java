package com.ryusoftwareinc.moffee.user.model;

public enum UserType {
	ADMIN, EMPLOYEE, REGULAR
}
