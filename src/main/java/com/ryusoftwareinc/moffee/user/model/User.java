package com.ryusoftwareinc.moffee.user.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.core.model.AbstractEntity;
import com.ryusoftwareinc.moffee.spot.model.Spot;

@Entity(name = "users")
public class User extends AbstractEntity {

	private static final UserType DEFAULT_USER_TYPE = UserType.REGULAR;

	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false)
	private String lastName;
	@Column(unique = true)
	private String email;
	
	@Column(nullable = false)
	private String hashedPassword;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private UserType type;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "owner_id")
	private Set<Car> cars;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "owner_id")
	private Set<Spot> spots;

	private Long referredBy;

	// goes up when charged or transferred Goes down when spent
	private double balance;
	// total charged from credit card
	private double totalCharged;
	// money earned that can be cashed out. can transfer this amt to balance.
	private double amtInBucket;
	// total earned from spots
	private double totalEarned;
	
	@OneToOne
	@JsonIgnore
	private StripeInfo stripeInfo;

	public User() {
		this.type = DEFAULT_USER_TYPE;
		this.cars = new HashSet<>();
		this.spots = new HashSet<>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public Set<Spot> getSpots() {
		return spots;
	}

	public void setSpots(Set<Spot> spots) {
		this.spots = spots;
	}

	public Long getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(Long referredBy) {
		this.referredBy = referredBy;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getTotalCharged() {
		return totalCharged;
	}

	public void setTotalCharged(double totalCharged) {
		this.totalCharged = totalCharged;
	}

	public double getAmtInBucket() {
		return amtInBucket;
	}

	@Deprecated
	public void setAmtInBucket(double amtInBucket) {
		this.amtInBucket = amtInBucket;
	}
	
	public void addToBucket(double amt) {
		this.amtInBucket += amt;
		this.totalEarned += amt;
	}

	public double getTotalEarned() {
		return totalEarned;
	}

	@Deprecated
	public void setTotalEarned(double totalEarned) {
		this.totalEarned = totalEarned;
	}
	
	public boolean add(Car car) {
		return cars.add(car);
	}
	
	public boolean add(Spot spot) {
		return spots.add(spot);
	}
	
	public boolean has(Spot spot) {
		return spots.contains(spot);
	}
	
	public boolean has(Car car) {
		return cars.contains(car);
	}

	public StripeInfo getStripeInfo() {
		return stripeInfo;
	}

	public void setStripeInfo(StripeInfo stripeInfo) {
		this.stripeInfo = stripeInfo;
	}

}
