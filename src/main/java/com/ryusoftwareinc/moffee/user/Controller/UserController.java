package com.ryusoftwareinc.moffee.user.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ryusoftwareinc.moffee.core.controller.AbstractBaseController;
import com.ryusoftwareinc.moffee.core.http.HttpResponse;
import com.ryusoftwareinc.moffee.user.model.ParkerDto;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@RequestMapping("/user")
@RestController
@CrossOrigin
public class UserController extends AbstractBaseController {
	
	@Autowired private UserService userService;
	
	// TODO: Remove
	@GetMapping("/getAll")
	public HttpResponse testGetAll() {
		List<User> users = new ArrayList<>();
		for (User user : userService.findAll()) {
			users.add(user);
		}
		return formResponse(users, "TEST");
	}
	
	@GetMapping("get")
	public HttpResponse get(@RequestParam String key) {
		User user = userService.findByKey(key);
		user.getSpots().stream().forEach(spot -> {
			if (spot.getParker() != null) {
				spot.setParkerDto(new ParkerDto(spot.getParker()));
			}
		});
		return formResponse(user, null);
	}
	
	@PostMapping("/register")
	public HttpResponse register(@RequestBody User user) {
		user = userService.save(user);
		return formResponse(user, "User created " + user.getKey());
	}
	
}
