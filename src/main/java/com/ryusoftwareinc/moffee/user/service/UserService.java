package com.ryusoftwareinc.moffee.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.repository.UserRepository;

@Service
@Transactional
public class UserService extends AbstractEntityService<User> {

	@Autowired
	private UserRepository userRepo;

	@Override
	public EntityRepository<User> getRepo() {
		return userRepo;
	}

	public boolean checkEmailExists(String email) {
		User user = userRepo.findByEmail(email);
		return user == null ? false : true;
	}
	
}
