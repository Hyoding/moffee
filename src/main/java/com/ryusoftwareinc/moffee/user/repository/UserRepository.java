package com.ryusoftwareinc.moffee.user.repository;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.user.model.User;

public interface UserRepository extends EntityRepository<User> {
	
	User findByEmail(String email);

}
