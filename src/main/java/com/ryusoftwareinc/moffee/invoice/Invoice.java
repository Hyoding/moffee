package com.ryusoftwareinc.moffee.invoice;

import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.moffee.core.model.AbstractEntity;
import com.ryusoftwareinc.moffee.core.util.DateTimeUtil;
import com.ryusoftwareinc.moffee.core.validator.ErrorCollector;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.user.model.User;

@Entity
public class Invoice extends AbstractEntity {

	@OneToOne
	@JsonIgnore
	private Spot spot;

	@OneToOne
	@JsonIgnore
	private User parker;

	private ZonedDateTime closedDate; // indicates meter timedOut

	private double agreedRate; // this is here so even though spot cost changes in the middle
	private long minutesPaid;
	private double total;

	private double providerCutPercent;
	private double providerCutAmt;

	private double serviceCutPercent;
	private double serviceCutAmt;

	public void addMinutes(long minutesPaid) {
		this.minutesPaid += minutesPaid;
	}

	public Spot getSpot() {
		return spot;
	}

	public void setSpot(Spot spot) {
		this.spot = spot;
	}

	public User getParker() {
		return parker;
	}

	public void setParker(User parker) {
		this.parker = parker;
	}

	public ZonedDateTime getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(ZonedDateTime closedDate) {
		this.closedDate = closedDate;
	}

	public double getAgreedRate() {
		return agreedRate;
	}

	public void setAgreedRate(double agreedRate) {
		this.agreedRate = agreedRate;
	}

	public long getMinutesPaid() {
		return minutesPaid;
	}

	public void setMinutesPaid(long minutesPaid) {
		this.minutesPaid = minutesPaid;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getSpotCutPercent() {
		return providerCutPercent;
	}

	public void setSpotCutPercent(double spotCutPercent) {
		this.providerCutPercent = spotCutPercent;
	}

	public double getProviderCutPercent() {
		return providerCutPercent;
	}

	public void setProviderCutPercent(double providerCutPercent) {
		this.providerCutPercent = providerCutPercent;
	}

	public double getProviderCutAmt() {
		return providerCutAmt;
	}

	public void setProviderCutAmt(double providerCutAmt) {
		this.providerCutAmt = providerCutAmt;
	}

	public double getServiceCutPercent() {
		return serviceCutPercent;
	}

	public void setServiceCutPercent(double serviceCutPercent) {
		this.serviceCutPercent = serviceCutPercent;
	}

	public double getServiceCutAmt() {
		return serviceCutAmt;
	}

	public void setServiceCutAmt(double serviceCutAmt) {
		this.serviceCutAmt = serviceCutAmt;
	}

	public double getServiceCharceAmt() {
		return serviceCutAmt;
	}

	public void setServiceCharceAmt(double serviceCharceAmt) {
		this.serviceCutAmt = serviceCharceAmt;
	}
	
	public void closeInvoice() {
		closedDate = DateTimeUtil.now();
		total = agreedRate * minutesPaid;
		providerCutAmt = total * providerCutPercent;
		serviceCutAmt = total * serviceCutPercent;
	}

	@Override
	public ErrorCollector collectErrors() {
		ErrorCollector ec = new ErrorCollector();
		if (agreedRate < 0) {
			ec.add("Rate cannot be below 0");
		}
		if (total < 0) {
			ec.add("Total cannot be below 0");
		}
		if (providerCutAmt < 0) {
			ec.add("Spot cut amt cannot be below 0");
		}
		if (serviceCutAmt < 0) {
			ec.add("Service cut amt cannot be below 0");
		}
		return ec;
	}

}
