package com.ryusoftwareinc.moffee.invoice;

import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.spot.model.Spot;

public interface InvoiceRepository extends EntityRepository<Invoice> {
	
	Invoice findBySpotAndClosedDateNull(Spot spot);
	
}
