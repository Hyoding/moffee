package com.ryusoftwareinc.moffee.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.config.Config;
import com.ryusoftwareinc.moffee.core.exception.ClientException;
import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.core.validator.ValidationException;
import com.ryusoftwareinc.moffee.spot.model.Spot;
import com.ryusoftwareinc.moffee.user.model.User;

@Service
@Transactional
public class InvoiceService extends AbstractEntityService<Invoice> {

	@Autowired
	private InvoiceRepository billRepo;
	
	@Autowired
	private Config config;
	
	@Override
	public EntityRepository<Invoice> getRepo() {
		return billRepo;
	}
		
	// TODO: Bug when charging additional doesn't take money away from user
	public Invoice chargeTime(Spot spot, long minToAdd) {	
		// look for existing invoice
		Invoice invoice = billRepo.findBySpotAndClosedDateNull(spot);
		if (invoice == null) {
			invoice = new Invoice();
			invoice.setSpot(spot);
			invoice.setParker(spot.getParker());
			invoice.setAgreedRate(spot.getRate());
			invoice.setSpotCutPercent(Double.parseDouble(config.getAmtSpotCutPercentage()));
			invoice.setServiceCutPercent(Double.parseDouble(config.getAmtServiceCutPercentage()));
		}
		// see if parker has balance
		User parker = spot.getParker();
		double cost = invoice.getAgreedRate() * (minToAdd / 60.0);
		double remainingBalance = parker.getBalance() - cost;
		boolean parkerCanPay = remainingBalance >= 0 ? true : false;
		if (!parkerCanPay) {
			throw new ClientException("Not enough money in your balance. Please charge your account");
		}
		
		parker.setBalance(remainingBalance);
		invoice.addMinutes(minToAdd);
		return save(invoice);
	}
	
	public Invoice closeInvoice(Spot spot) {
		Invoice invoice = billRepo.findBySpotAndClosedDateNull(spot);
		invoice.closeInvoice();
		User provider = spot.getOwner();
		provider.addToBucket(invoice.getProviderCutAmt());
		return invoice;
	}
	
	public Invoice findBySpotAndClosedDateNull(Spot spot) {
		return billRepo.findBySpotAndClosedDateNull(spot);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Invoice save(Invoice invoice) {
		if (invoice.getId() == null) { // insert
			Invoice existing = findBySpotAndClosedDateNull(invoice.getSpot());
			if (existing != null) {
				throw new ValidationException("We cannot have more than one invoice for a spot");
			}
		}
		return super.save(invoice);
	}

}
