package com.ryusoftwareinc.moffee.car.model;

import com.ryusoftwareinc.moffee.core.http.BaseHttpRequest;

public class RegisterCarRequest extends BaseHttpRequest {
	
	private Car car;
	private Long userId;

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
