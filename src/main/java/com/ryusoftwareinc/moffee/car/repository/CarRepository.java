package com.ryusoftwareinc.moffee.car.repository;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.core.repository.EntityRepository;

public interface CarRepository extends EntityRepository<Car> {
	
}
