package com.ryusoftwareinc.moffee.car.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ryusoftwareinc.moffee.car.model.Car;
import com.ryusoftwareinc.moffee.car.repository.CarRepository;
import com.ryusoftwareinc.moffee.core.repository.EntityRepository;
import com.ryusoftwareinc.moffee.core.service.AbstractEntityService;
import com.ryusoftwareinc.moffee.notification.model.Email;
import com.ryusoftwareinc.moffee.notification.service.NotificationService;
import com.ryusoftwareinc.moffee.user.model.User;
import com.ryusoftwareinc.moffee.user.service.UserService;

@Service
@Transactional
public class CarService extends AbstractEntityService<Car> {
	
	@Autowired
	private CarRepository carRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NotificationService notifService;
	
	@Value("${spring.mail.username}")
	private String emailFrom;

	@Override
	public EntityRepository<Car> getRepo() {
		return carRepository;
	}

	public void registerCar(Car car, Long userId) {
		User user = userService.findById(userId).get();
		car.setOwner(user);
		car = carRepository.save(car);
		
		// TODO: set up customer in stripe
//		String stripeCustomerId = stripeService.createCustomer(user);
//		user.getStripeInfo().setCustomerId(stripeCustomerId);
		
		notifService.sendEmail(generateEmailRegistered(car, user));
	}

	public void removeCar(String carKey) {
		Car car = findByKey(carKey);
		car.setEnabled(false);
	}
	
	private Email generateEmailRegistered(Car car, User user) {
		Email email = new Email();
		email.setSubject("Car Registered for " + car.getKey());
		email.setFrom(emailFrom);
		email.addTo(user.getEmail());
		email.addBcc(emailFrom);

		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + user.getFirstName() + ",\n\n");
		sb.append("Thank you for registering your car\n");
		sb.append("Please make sure your information is up to date");
		email.setText(sb.toString());

		return email;
	}

}
