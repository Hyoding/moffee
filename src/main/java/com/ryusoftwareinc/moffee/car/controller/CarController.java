package com.ryusoftwareinc.moffee.car.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ryusoftwareinc.moffee.car.model.RegisterCarRequest;
import com.ryusoftwareinc.moffee.car.service.CarService;
import com.ryusoftwareinc.moffee.core.controller.AbstractBaseController;
import com.ryusoftwareinc.moffee.core.http.HttpResponse;

@RequestMapping("/car")
@RestController
@CrossOrigin
public class CarController extends AbstractBaseController {

	@Autowired
	private CarService carService;

	@PostMapping("/register")
	public HttpResponse register(@RequestBody RegisterCarRequest request) {
		carService.registerCar(request.getCar(), request.getUserId());
		return formResponse(null, "Car registered for " + request.getCar().getKey());
	}
	
	@GetMapping("/remove")
	public HttpResponse removeCar(@RequestParam String formKey) {
		carService.removeCar(formKey);
		return formResponse(null, "Car removed for for " + formKey);
	}
	
}
